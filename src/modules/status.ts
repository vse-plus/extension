import { Authentication } from "../types";

export const enableVsePlusStatusBar = (authentication: Authentication) => {
    // Do not display VŠE+ status in testing modules
    if (window.location.href.includes("/auth/elis/")) {
        return;
    }

    const container = document.createElement("div");
    const login = authentication.authenticated ? `Přihlášen ${authentication.username}` : "Bez přihlášení"

    container.classList.add("status");
    container.innerHTML = `
        <div class="status-info">
            <img src="https://avatars.githubusercontent.com/u/108870700?s=200&v=4" width="48" height="48">
            <div>
                <div class="status-header">VŠE+ aktivní</div>
                <strong class="status-login">
                    ${login}
                </strong>
            </div>
        </div>
        <a href="https://gitlab.com/vse-plus/extension/-/issues" target="_blank">Nahlásit chybu</a>
    `;

    document.body.appendChild(container);
};

export const displayNonCzechLanguageWarning = () => {
    const cz = Array.from(document.querySelectorAll<HTMLAnchorElement>("#vlajky a")).every(link => link.innerText !== "ČESKY");

    if (cz) {
        return;
    }

    const container = document.createElement("div");

    container.classList.add("language-warning");
    container.innerHTML = `
        <strong>You are using a non-Czech version of InSIS.</strong><br>
        While most of the features added by VŠE+ should work as expected, please note that there might be some issues caused by eg. inconsistent date formats.
    `;

    document.body.appendChild(container);
};
