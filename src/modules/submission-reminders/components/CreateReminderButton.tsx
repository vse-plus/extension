import moment, { Moment } from "moment";
import { Box, Button, Popover, PopoverArrow, PopoverBody, PopoverContent, PopoverTrigger, Tooltip } from "@chakra-ui/react";
import React from "react";
import { FC } from "react"

export interface CreateReminderButtonsProps {
    due: Date,
    createReminder: (reminder: Date) => void
}

export interface ButtonDefinition {
    label: string,
    date: Moment
}

export const CreateReminderButtons: FC<CreateReminderButtonsProps> = ({ due, createReminder }: CreateReminderButtonsProps) => {
    const now = moment();
    const base = moment(due).locale("cs");
    const definitions: Array<ButtonDefinition> = [
        {
            label: "Týden předem",
            date: base.clone().subtract(1, "week")
        },
        {
            label: "2 dny předem",
            date: base.clone().subtract(2, "days")
        },
        {
            label: "Den předem",
            date: base.clone().subtract(1, "day")
        },
        {
            label: "12 hodin předem",
            date: base.clone().subtract(12, "hours")
        },
        {
            label: "6 hodin předem",
            date: base.clone().subtract(6, "hours")
        }
    ];

    return (
        <Box display="flex" flexDirection="row" flexWrap="wrap" justifyContent="center" alignItems="center">
            {definitions.map((definition, i) =>
                <Tooltip label={definition.date.isBefore(now) ? "Tento čas je již v minulosti" : definition.date.format("LLL")} key={i}>
                    <Button display="block" margin="2" disabled={definition.date.isBefore(now)} onClick={() => createReminder(definition.date.toDate())} colorScheme="blue">{definition.label}</Button>
                </Tooltip>
            )}
        </Box>
    );
};