export const parseDate = (raw: string): Date => {
    // 10. 11. 2022 -> 10/10/2022 (this format is used in the english insis version)
    const replaced = raw.replace(/\.\s+/g, "/")
    const [day, month, year] = replaced.split("/").map(Number);

    return new Date(year, month - 1, day);
};

export const parseDateTime = (raw: string): Date => {
    // 10. 11. 2022 -> 10/10/2022 (this format is used in the english insis version)
    const replaced = raw.replace(/\.\s+/g, "/")
    const [date, time] = replaced.split(/\s+/);

    const [day, month, year] = date.split("/").map(Number);
    const [hours, minutes] = time.split(":").map(Number);

    return new Date(year, month - 1, day, hours, minutes);
};

export const convertDateToString = (date: Date): string => {
    // This is the format, that the java endpoint expects to receive
    const pad = (source: number): string => source.toString().padStart(2, "0");

    return `${pad(date.getFullYear())}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}T${pad(date.getHours())}:${pad(date.getMinutes())}`;
};


export const formatDate = (date: Date): string => {
    // The format displayed to user
    const pad = (source: number): string => source.toString().padStart(2, "0");
    return `${date.getDate()}. ${date.getMonth() + 1}. ${date.getFullYear()} ${pad(date.getHours())}:${pad(date.getMinutes())}`;
};