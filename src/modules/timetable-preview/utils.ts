import { AvailableTimetableEntry, RegisteredTimetableEntry, RegistrationTimetableEntry, TimetableEntryLayout, TimetableEntryType } from "./types";

export const days = {
    short: ["Po", "Út", "St", "Čt", "Pá"],
    long: ["Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek"]
};

export const parseTimeToMinute = (time: string): number => {
    const parts = time.split(":").map(Number);
    const [hours, minutes] = parts;

    return hours * 60 + minutes;
};

export const parseTimetableEntry = (text: string, link: string | null): RegistrationTimetableEntry | null => {
    const pattern = /(Př|Cv)\s+(Po|Út|St|Čt|Pá)\s+(\d{2}:\d{2})\s*-\s*(\d{2}:\d{2})/;
    const matches = text.match(pattern);

    if (matches == null) {
        return null;
    }

    const [_, type, day, start, end] = matches;

    return {
        type: type == "Př" ? TimetableEntryType.Lecture : TimetableEntryType.Seminar,
        day: days.short.indexOf(day),
        start: parseTimeToMinute(start),
        end: parseTimeToMinute(end),
        time: `${start} - ${end}`,
        link: link
    };
};

export const parseTimetableEntries = (html: string): Array<RegistrationTimetableEntry> => {
    return html
        .split("<br>")
        .map(entry => {
            const [_, parameters] = entry.match(/href=".*?\?(.*?)"/) ?? [];
            const link = parameters !== undefined ? (window.location.href.split("?")[0] + "?" + parameters) : null;

            const stripped = entry.replace(/<a.*?>/, "").replace(/<\/a>/, "");

            return parseTimetableEntry(stripped, link);
        })
        .filter(entry => entry != null) as Array<RegistrationTimetableEntry>;
};

export const parseTimetable = (html: string): Array<RegisteredTimetableEntry> => {
    const element = document.createElement("div");

    document.body.appendChild(element);

    element.innerHTML = html;
    element.style.display = "none";

    const n = html.indexOf(". kolo)</title>") == -1 ? 2 : 3;

    const table = element.querySelector(`table:nth-of-type(${n})`);
    const rows = Array.from(table?.querySelectorAll("tr") ?? []) as Array<HTMLTableRowElement>;
    const subjects = rows.flatMap((row, index) => {
        // Do not parse the header
        if (index == 0) {
            return null;
        }

        const cells = Array.from(row.querySelectorAll("td")) as Array<HTMLTableCellElement>;

        return parseTimetableEntries(cells[12].innerHTML)
            .filter(element => element != null)
            .map(entry => ({ ...entry, code: cells[2].innerText, name: cells[3].innerText }));
    });

    document.body.removeChild(element);

    return subjects.filter(element => element != null) as Array<RegisteredTimetableEntry>;
};

export const loadCurrentTimetable = async () => {
    const source = await fetch("https://insis.vse.cz/auth/student/registrace.pl?lang=cz")
        .then(response => response.text())
        .then(response => parseTimetable(response));

    return source;
};

export const parseAvailableEntries = (source: Array<RegisteredTimetableEntry>, hideCollisions: boolean): Array<AvailableTimetableEntry> => {
    const items = Array.from(document.querySelectorAll("tr:not(.zahlavi)")) as Array<HTMLTableRowElement>;
    const availableItems: Array<AvailableTimetableEntry> = [];

    items.forEach((item, index) => {
        const cells = Array.from(item.querySelectorAll("td.odsazena")) as Array<HTMLTableCellElement>;
        const day = days.long.indexOf(cells[1].innerText.trim());
        const time = cells[2].innerText.trim();

        const [start, end] = time.split("-").map(parseTimeToMinute);

        const hasCollision = source.some(other =>
            other.day == day && (
                (other.start < end && other.end >= end) ||
                (other.start < start && other.end >= start)
            )
        );

        if (hasCollision && !hideCollisions) {
            item.style.opacity = "0.35";
        }

        availableItems.push(
            {
                index: index,
                day: day,
                start: start,
                end: end,
                collision: hasCollision,
                time: time,
                highlighted: false,
                type: TimetableEntryType.Unknown,
                link: null
            }
        );

        item.setAttribute("data-timetable-highlight", index.toString());
    });

    return availableItems;
}

export const computeEntryLayout = (start: number, end: number): TimetableEntryLayout => {
    const from = 7 * 60 + 30; // 7:30
    const to = 19 * 60 + 30; // 19:30

    return {
        offset: (start - from) / (to - from) * 100,
        width: (end - start) / (to - from) * 100
    }
}
