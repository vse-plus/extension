import { Moment } from "moment"

export enum TimetableEntryType {
    Lecture = "Lecture",
    Seminar = "Seminar",
    Other = "Other"
}

export type ParsedTimetableEntryLink = {
    href: string,
    label: string
}

export type ParsedTimetableEntry = {
    room: ParsedTimetableEntryLink,
    course: ParsedTimetableEntryLink,
    teacher: ParsedTimetableEntryLink,
    day: number,  // in days, since monday
    start: number, // in minutes, since midnight,
    duration: number, // in minutes
    type: TimetableEntryType,
    daysOff: Array<Date>,
    eventDates: Array<Date>,
    notes: string
}

export type ParsedSemesterInterval = {
    start: Moment,
    end: Moment
}

export type ParsedTimetable = {
    entries: Array<ParsedTimetableEntry>
    start: number,
    end: number,
    semester: ParsedSemesterInterval | null
}

export type TimetableEvent = {
    id: number,
    type: string,
    course: string,
    note: string,
    date: Moment
}