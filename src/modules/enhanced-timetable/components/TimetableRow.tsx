import moment from "moment";
import { Moment } from "moment";
import React, { useEffect, useMemo, useState } from "react";
import { FC } from "react";
import { collides } from "../../../utils";
import { ParsedTimetableEntry, TimetableEvent } from "../types";
import { TimetableEntry } from "./TimetableEntry";

export interface TimetableRowProps {
    date: Moment,
    entries: Array<ParsedTimetableEntry>,
    events: Array<TimetableEvent>,
    start: number,
    end: number,
    onSelect?: (entry: ParsedTimetableEntry, date: Moment) => void
}

type Lane = Array<ParsedTimetableEntry>;

const containsCollision = (lane: Lane, entry: ParsedTimetableEntry): boolean => {
    const convert = (input: ParsedTimetableEntry): { start: number, end: number } => {
        return {
            start: input.start,
            end: input.start + input.duration
        }
    }

    return lane.some(existing => collides(convert(existing), convert(entry)));
};

export const TimetableRow: FC<TimetableRowProps> = ({ date, entries, events, start, end, onSelect }: TimetableRowProps) => {
    // Split entries to different lanes to support displaying collisions
    const lanes = useMemo<Array<Lane>>(
        () => entries.reduce(
            (lanes, current) => {
                // First find a lane, that does not contain a collision
                // If no such lane is found, create a new one
                const index = lanes.findIndex(lane => !containsCollision(lane, current));
                const found = index !== -1;

                const updated = found ? lanes : lanes.concat([[]]);
                const selected = found ? updated[index] : updated[updated.length - 1];

                selected.push(current);

                return updated;
            },
            [] as Array<Lane>
        ),
        [entries]
    );

    const range = (end - start);
    const today = moment().isSame(date, "days");

    const [indicatorOffset, setIndicatorOffset] = useState(0);

    useEffect(() => {
        if (today) {
            const update = () => {
                const now = moment();
                const time = now.hours() * 60 + now.minutes() - start;

                setIndicatorOffset(Math.max(Math.min(time / range, 1), 0));
            }

            update();

            const interval = setInterval(update, 30000);
            return () => window.clearInterval(interval);
        }
    }, [today]);

    return (
        <div className={`timetable-row ${today && "timetable-row--today"}`}>
            <div className="timetable-row__header">
                <div className="timetable-row__header--weekday">{date.format("dd").toUpperCase()}</div>
                <div className="timetable-row__header--date">
                    <span className="timetable-row__header--date-day">{date.format("DD.")}</span>
                    <span>{date.format("MMMM")}</span>
                </div>
            </div>

            <div className="timetable-row__lanes">
                {lanes.map((lane, i) =>
                    <div key={i} className="timetable-lane">
                        {lane.map((entry, j) =>
                            <TimetableEntry
                                key={j}
                                start={date.clone().add(entry.start, "minutes")}
                                end={date.clone().add(entry.start + entry.duration, "minutes")}
                                data={entry}
                                events={events}
                                onClick={() => onSelect && onSelect(entry, date)}
                                style={{
                                    left: ((entry.start - start) / range) * 100 + "%",
                                    width: (entry.duration / range) * 100 + "%"
                                }}
                            />
                        )}
                    </div>
                )}

                {
                    (today && (indicatorOffset > 0 && indicatorOffset < 1)) &&
                    <>
                        <div className="timetable-row__indicator" style={{ width: (indicatorOffset * 100) + "%" }} />
                        <div className="timetable-row__indicator--line" style={{ left: (indicatorOffset * 100) + "%" }} />
                    </>
                }
            </div>

        </div>
    );
};
