import { Box, Button, ButtonGroup, Tooltip, Text } from "@chakra-ui/react";
import { Moment } from "moment";
import React from "react";
import { FC } from "react";
import { ParsedSemesterInterval } from "../types";

export interface TimetableNavigationProps {
    monday: Moment,
    sunday: Moment,
    semester: ParsedSemesterInterval | null,
    previous: () => void,
    next: () => void,
    reset: () => void,
}

export const TimetableNavigation: FC<TimetableNavigationProps> = ({ monday, sunday, previous, next, reset, semester }: TimetableNavigationProps) => {
    return (
        <Box className="timetable__navigation">
            <ButtonGroup>
                <Tooltip label="Předchozí týden">
                    <Button onClick={() => previous()} colorScheme="blue" variant="ghost">&laquo;</Button>
                </Tooltip>
                <Tooltip label="Aktuální týden">
                    <Button onClick={() => reset()} colorScheme="blue" variant="ghost">🗓️</Button>
                </Tooltip>

                <Text className="timetable__current-week">
                    {monday.format("LL")} - {sunday.format("LL")}
                </Text>
                { semester !== null && (
                        <Text className="timetable__current-week-number">
                            {
                                (monday.isBefore(semester.start) || monday.isAfter(semester.end))
                                ? "Mimo výukové období"
                                : `${monday.diff(semester.start, "weeks") + 1}. týden` 
                            }
                        </Text>
                    )
                }

                <Tooltip label="Další týden">
                    <Button onClick={() => next()} colorScheme="blue" variant="ghost">&raquo;</Button>
                </Tooltip>
            </ButtonGroup>
        </Box>
    );
};