import type { ParsedTimetable } from '../types';
import type { ButtonProps } from '@chakra-ui/react';

import React from 'react';
import getICalendar from '../calendar-exporter';
import browser from 'webextension-polyfill';
import { Button, Tooltip } from '@chakra-ui/react';

export type ExportButtonProps = ButtonProps & {
    data: ParsedTimetable;
};

function handleDownload(data: ParsedTimetable) {
    const iCalResult = getICalendar(data);

    if (iCalResult.error) {
        console.error(iCalResult.error);
        return;
    }

    void browser.runtime.sendMessage({ id: 'vse-plus-download-ics', value: iCalResult.value });
}

export default function ExportButton({ data, onClick, title, ...rest }: ExportButtonProps) {
    return (
        <Tooltip label={title}>
            <Button type="button" onClick={() => void handleDownload(data)} {...rest} />
        </Tooltip>
    );
}