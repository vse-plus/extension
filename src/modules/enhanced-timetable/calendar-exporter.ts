import type { ParsedSemesterInterval, ParsedTimetable, ParsedTimetableEntry } from './types';
import type { DateArray, EventAttributes, ReturnObject as CalendarEventsReturn } from 'ics';

import 'moment-timezone';
import { createEvents } from 'ics';
import { Frequency, RRule, RRuleSet } from 'rrule';
import { TimetableEntryType } from "./types";

function createRecurrenceRules(entry: ParsedTimetableEntry, recurrenceEnd: Date): RRuleSet {
    const ruleSet = new RRuleSet();

    // Does not have recurring dates -> just pass in the known ones
    if (entry.eventDates.length > 0) {
        for (const eventDate of entry.eventDates) {
            ruleSet.rdate(eventDate);
        }

        return ruleSet;
    }

    ruleSet.rrule(new RRule({
        freq: Frequency.WEEKLY,
        until: recurrenceEnd,
    }));

    for (const dayOff of entry.daysOff) {
        ruleSet.exdate(dayOff);
    }

    return ruleSet;
}

function translateEntryType(entryType: TimetableEntryType) {
    switch (entryType) {
        case TimetableEntryType.Lecture: return 'Přednáška';
        case TimetableEntryType.Seminar: return 'Cvičení';
        case TimetableEntryType.Other: return 'Jiné';
    }
}

function createICalendarEvent(entry: ParsedTimetableEntry, { start, end }: ParsedSemesterInterval): EventAttributes {
    let startTime = start.clone()
        .add(entry.day, 'days')
        .add(entry.start, 'minutes')
        .tz('Europe/Prague');

    let recurrences = createRecurrenceRules(entry, end.toDate())?.toString();

    if (recurrences) {
        // avoid duplication of the key RRULE:
        recurrences = recurrences.split(':').slice(1).join(':');
    }

    return {
        productId: 'VŠE+',
        title: entry.course.label,
        location: entry.room.label,
        start: startTime.format('YYYY-M-D-H-m').split("-").map(Number) as DateArray,
        duration: { minutes: entry.duration },
        recurrenceRule: recurrences?.toString(),
        description: `${translateEntryType(entry.type)}\n${entry.teacher.label}`,
    };
}

export default function getICalendar(data: ParsedTimetable): CalendarEventsReturn {
    if (!data.semester?.start || !data.semester.end) {
        throw new Error('Didn\'t receive information about semester');
    }

    const calendarEvents: EventAttributes[] = [];

    for (const entry of data.entries) {
        calendarEvents.push(createICalendarEvent(entry, data.semester));
    }

    return createEvents(calendarEvents);
}
