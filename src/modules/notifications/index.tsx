import React from "react";
import { createRoot } from "react-dom/client";
import browser from "webextension-polyfill";
import NotificationsOverlay from "./components/NotificationsOverlay";

const dismissNotifications = async () => {
    await browser.storage.local.set({"notificationsTimestamp": new Date().getTime()});
};

const createContainer = (): HTMLDivElement => {
    const id = "notifications-overlay";
    const container = document.createElement("div");

    container.id = id;
    document.body.appendChild(container);

    return container;
};

export const showNotifications = async (timestamp: Date) => {
    const container = createContainer();
    const root = createRoot(container);

    root.render(
        <React.StrictMode>
            <NotificationsOverlay timestamp={timestamp} dismissNotifications={dismissNotifications} />
        </React.StrictMode>
    );
};