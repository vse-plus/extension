import { Divider, Heading, Text } from "@chakra-ui/react";
import React, { FC } from "react";

interface UserProps {
    username: string
}

const User: FC<UserProps> = ({ username }: UserProps) => {
    return (
        <>
            <Heading as="h1" size="lg">VŠE+</Heading>

            <Divider/>

            <Text>
                Přihlášení pod účtem <strong>{username}@vse.cz</strong> je aktivní.<br />
            </Text>
        </>
    );
};

export default User;