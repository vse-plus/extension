import React, { FC, useContext, useState } from "react";
import { PopupContext } from "../popup";
import styled from "styled-components";
import { Alert, AlertDescription, AlertIcon, Button, ButtonGroup, Divider, Heading, HStack, Input, InputGroup, InputRightAddon } from "@chakra-ui/react";
import { createAccount, verifyAccount } from "../api";
import { useEffect } from "react";


const UsernameInputWrapper = styled.div`
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-around;
    align-items: center;
    padding: 1rem;
`;

const Paragraph = styled.p`
   padding: 0.5rem 0rem;
`;

interface UsernamePromptProps {
    loading: boolean,
    submitUsername: (username: string) => void
}

const UsernamePrompt: FC<UsernamePromptProps> = ({ submitUsername, loading }: UsernamePromptProps) => {
    const [username, setUsername] = useState<string>("");
    const valid = /^[a-z]{4}[0-9]{2}$/.test(username)

    return (
        <>
            <Paragraph>Po přihlášení bude dostupná funkcionalita připomenutí odevzdáváren</Paragraph>
            <Paragraph>Pro přihlášení stačí zadat školní username a na email ti bude zaslán 8 místný ověřovací kód.</Paragraph>

            <Divider />

            <UsernameInputWrapper>
                <InputGroup>
                    <Input type="text" value={username} disabled={loading} onChange={(event) => setUsername(event.target.value)} minLength={6} maxLength={6} />
                    <InputRightAddon>@vse.cz</InputRightAddon>
                </InputGroup>
                <ButtonGroup>
                    <Button isLoading={loading} onClick={() => submitUsername(username)} disabled={!valid} colorScheme="telegram" style={{ marginLeft: "1rem" }}>Zaslat kód</Button>
                </ButtonGroup>
            </UsernameInputWrapper>
        </>
    );
}

interface VerificationCodePromptProps {
    loading: boolean,
    username: string,
    submitVerificationCode: (username: string, code: string) => void
}

const VerificationCodePrompt: FC<VerificationCodePromptProps> = ({ loading, username, submitVerificationCode }: VerificationCodePromptProps) => {
    const [code, setCode] = useState<string>("");
    const valid = code.length === 8 && /^[A-Z0-9]+$/.test(code);

    return (
        <>
            <Alert>
                <AlertIcon />
                <AlertDescription>
                    Na adresu {" "}<strong>{username}@vse.cz</strong>{" "} byl odeslán ověřovací kód. <br />
                    <small>Občas se stane, že emaily mimo vse.cz doménu skončí ve spamu.</small>
                </AlertDescription>
            </Alert>

            <Divider />

            <HStack justifyContent="center" padding="3">
                <Input value={code} spellCheck={false} onChange={(event) => setCode(event.target.value.toUpperCase())} disabled={loading} fontFamily="monospace" textAlign="center" letterSpacing="1rem" maxLength={8} placeholder="XXXXXXXX" />
            </HStack>

            <HStack alignItems="center" justifyContent="center">
                <ButtonGroup>
                    <Button colorScheme="telegram" disabled={!valid} isLoading={loading} onClick={() => submitVerificationCode(username, code)}>Dokončit přihlášení</Button>
                </ButtonGroup>
            </HStack>
        </>
    );
}

const Login: FC = () => {
    const context = useContext(PopupContext);
    const [loading, setLoading] = useState<boolean>(false);
    const [awaitingVerification, setAwaitingVerification] = useState<boolean>(false);

    useEffect(() => setAwaitingVerification((context.authentication?.username ?? null) !== null), [context.authentication]);

    const submitUsername = (username: string) => {
        setLoading(true);
        createAccount(username)
            .then(response => {
                setLoading(false);
                setAwaitingVerification(true);

                context.setAuthentication({
                    username: response.username,
                    authenticated: false,
                    token: null
                });
            })
            .catch(() => {
                setLoading(false);
                window.alert("Během komunikace se serverem došlo k chybě")
            });
    };

    const submitVerificationCode = (username: string, code: string) => {
        setLoading(true);
        verifyAccount(username, code)
            .then(response => {
                setLoading(false);
                setAwaitingVerification(true);

                context.setAuthentication({
                    username: response.username,
                    token: response.token,
                    authenticated: true,
                });
            })
            .catch(() => {
                setLoading(false);
                window.alert("Neplatný verifikační kód")
            });
    };

    const username = context.authentication?.username ?? "";

    return (
        <>
            <Heading as="h1" size="lg">Přihlášení do VŠE+</Heading>
            {
                awaitingVerification
                    ? <VerificationCodePrompt username={username} submitVerificationCode={submitVerificationCode} loading={loading} />
                    : <UsernamePrompt submitUsername={submitUsername} loading={loading} />
            }
        </>
    );
};

export default Login;