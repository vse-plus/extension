import browser from "webextension-polyfill";

browser.runtime.onMessage.addListener((message) => {
    if (message.id === 'vse-plus-download-ics') {
        const blob = new Blob([message.value], { type: 'text/calendar' });

        const reader = new FileReader();
        reader.readAsDataURL(blob);

        reader.addEventListener('loadend', function () {
            return browser.downloads.download({
                url: reader.result as string,
                filename: 'timetable.ics',
                saveAs: true,
            });
        });
    }
});